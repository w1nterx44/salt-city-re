<?php

namespace AppBundle\Etc;

class VkMini
{
    protected $groupId;
    public $members;
    public $friends;
    public $group;
    public $user;
    const COUNT = 32;

    public function __construct($groupId)
    {
        $this->groupId = $groupId;
        $this->group = json_decode(file_get_contents("https://api.vkontakte.ru/method/groups.getById?group_id=$this->groupId&fields=members_count"), true);
        if (!isset($this->group['error'])) {
            $max = $this->group['response'][0]['members_count'];
            //$count = 50;
            if ($max > self::COUNT) {
                $offset = rand(0, ($max - self::COUNT - 1));
            } else {
                $offset = 0;
            }
            $this->members = json_decode(file_get_contents("https://api.vkontakte.ru/method/groups.getMembers?group_id=$this->groupId&fields=photo_50&count=".self::COUNT."&offset=$offset"), true);
            // $this->friends = json_decode(file_get_contents("https://api.vkontakte.ru/method/groups.getMembers?group_id=$this->groupId&fields=photo_50&count=$count&filter=friends"), true);
            // $this->user = json_decode(file_get_contents("https://api.vkontakte.ru/method/users.get?fields=photo_50"), true);
        } else {
            echo $this->group['error']['error_msg'];
        }
    }
    public function getGroupName()
    {
        return $this->group['response'][0]['name'];
    }
    public function getGroupPhoto()
    {
        return $this->group['response'][0]['photo'];
    }
    public function getMembersCount()
    {
        return $this->members['response']['count'];
    }
    public function getMembersUsers()
    {
        // if (isset($this->friends['response']['count']) && $this->friends['response']['count'] > 0) {
        //     return $this->friends['response']['users'];
        // } elseif (isset($this->members['response']['count']) && $this->members['response']['count'] > 0) {
        //     return $this->members['response']['users'];
        // }
        if (isset($this->members['response']['count']) && $this->members['response']['count'] > 0) {
            return $this->members['response']['users'];
        }
        return false;
    }
    public function outputDiv()
    {
        $out = "
            <div class='vk_mini_out'>
                <div class='container text-left'>
                    <h4><img src='{{ asset('images/vk_mini/vkflat.png') }}' height=20px width=auto /> группа ".$this->getGroupName().", ".$this->getMembersCount()." ".self::getNumEnding($this->getMembersCount(), array('участник', 'участника', 'участников'))."</h4>
                </div>
        ";
        $out .= "   <div class='vk_mini_photos'>";
        // $users = $this->getMembersUsers();
        // for ($i = 0, $i < self::COUNT, $i++) {
        //     $out .= "
        //                 <img src='".$users[$i]['photo_50']."' style='padding-left: 4px;'/>
        //     ";
        // }
        $cnt = 0;
        foreach($this->getMembersUsers() as $i) {
            if ($cnt == self::COUNT) {
                break;
            }
            $out .= "
                        <img src='".$i['photo_50']."'/>
            ";
            $cnt++;
        }
        $out .= "   </div>";
        $out .= "
                    <a href='https://vk.com/club".$this->groupId."' target='blank'>
                        <span></span>
                    </a>
        ";
        $out .= "</div>";
        return $out;
    }
    public static function getNumEnding($number, $endingArray)
    {
        $number = $number % 100;
        if ($number>=11 && $number<=19) {
            $ending=$endingArray[2];
        }
        else {
            $i = $number % 10;
            switch ($i)
            {
                case (1): $ending = $endingArray[0]; break;
                case (2):
                case (3):
                case (4): $ending = $endingArray[1]; break;
                default: $ending=$endingArray[2];
            }
        }
        return $ending;
    }
}
